package utills;

import myexceptions.CredentialException;

import javax.security.auth.login.CredentialNotFoundException;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class MainUtils {
    // credentials
    final static private String USERNAME = "admin";
    final  static private  String PASSWORD = "admin@cstad";
   // FileNotFoundException is the subclass of the IOException
    public void printingTextFromFile() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new FileReader("/var/hellworld.txt")
        );
        System.out.println(bufferedReader.readLine());
    }
   public static float division(int a, int b) {
        if (a < 0)
            throw new ArithmeticException("A must be positive number!");
        if (b == 0)
            throw new ArithmeticException("Cannot be divided by zero!! ");
        return a / (float) b;
    }


    public  static  boolean login(String username, String password) throws  CredentialException{
        boolean isCredentialValid = (username.equals(USERNAME) && password.equals(PASSWORD));
        if (!isCredentialValid)
              throw new CredentialException("Username or Password is incorrect!!");
        return true;
    }


    public  static int readInteger(String message, Scanner input ){
        while(true){
            try {
                System.out.println(message);
               return Integer.parseInt(input.nextLine());
            } catch (NumberFormatException ex) {
                System.out.println("ERROR>> please input  number only!! ");
            }
        }
    }


}
